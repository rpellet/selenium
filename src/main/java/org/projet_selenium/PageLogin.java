package org.projet_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageLogin {
	@FindBy (xpath="//input[@name='username']")
	WebElement field_username;
	
	@FindBy (xpath="//input[@name='password']")
	WebElement field_password;
	
	@FindBy (xpath="//input[@name='signon']")
	WebElement button_login;
	
	public Homepage Login (WebDriver driver, String username, String password) {
		field_username.clear();
		field_username.sendKeys(username);
		
		field_password.clear();
		field_password.sendKeys(password);
		
		button_login.click();
		
		return PageFactory.initElements(driver, Homepage.class);
	}
}