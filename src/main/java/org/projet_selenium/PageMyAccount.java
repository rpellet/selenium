package org.projet_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class PageMyAccount {
	@FindBy (xpath="//select[@name='account.languagePreference']")
	WebElement menu_languagePreference;

	@FindBy (xpath="//select[@name='account.favouriteCategoryId']")
	WebElement menu_favouriteCategory;
	
	@FindBy (xpath="//input[@name='account.listOption']")
	WebElement box_enableMyList;
	
	@FindBy (xpath="//input[@name='account.bannerOption']")
	WebElement box_enableMyBanner;
	
	@FindBy (xpath="//input[@value='Save Account Information']")
	WebElement button_saveAccountInformation;
	
	public void changeLanguageTo (WebDriver driver, String language) {
		Select selectLanguage = new Select(menu_languagePreference);
		selectLanguage.selectByValue(language);
	}
	
	public void changeFavouriteCategoryTo (WebDriver driver, String category) {
		Select selectCategory = new Select(menu_favouriteCategory);
		selectCategory.selectByValue(category);
	}
	
	public void clickBoxEnableMyList (WebDriver driver) {
		box_enableMyList.click();
	}
	
	public String getSelectedLanguage(WebDriver driver) {
		Select selectLanguage = new Select(menu_languagePreference);
		return selectLanguage.getAllSelectedOptions().get(0).getText();
	}
	
	public String getFavouriteCategory(WebDriver driver) {
		Select selectCategory = new Select(menu_favouriteCategory);
		return selectCategory.getAllSelectedOptions().get(0).getText();
	}
	
	public void clickButtonSaveAccountInformation (WebDriver driver) {
		button_saveAccountInformation.click();
	}
	
	
}