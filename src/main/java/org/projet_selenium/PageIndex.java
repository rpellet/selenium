package org.projet_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageIndex {
	@FindBy (xpath="//a[.='Sign In']")
	WebElement button_signin;
	
	public PageLogin clickButtonSignin(WebDriver driver) {
		button_signin.click();
		return PageFactory.initElements(driver, PageLogin.class);
	}
}