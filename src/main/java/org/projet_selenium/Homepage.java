package org.projet_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homepage {
	@FindBy (xpath="//div[@id='SidebarContent']/a[contains(@href,'categoryId=FISH')]")
	WebElement button_fish;
	
	@FindBy (xpath="//a[.='My Account']")
	WebElement button_myAccount;
	
	public PageFish clickButtonFish (WebDriver driver) {
		button_fish.click();
		return PageFactory.initElements(driver, PageFish.class);
	}
	
	public PageMyAccount clickButtonMyAccount (WebDriver driver) {
		button_myAccount.click();
		return PageFactory.initElements(driver, PageMyAccount.class);
	}
}