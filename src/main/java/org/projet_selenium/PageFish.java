package org.projet_selenium;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageFish {
	@FindBy (xpath="//a[.='FI-SW-01']")
	WebElement button_angelfish;
	
	@FindBy (xpath="//a[.='FI-SW-02']")
	WebElement button_tigerShark;
	
	@FindBy (xpath="//a[.='FI-FW-01']")
	WebElement button_koi;

	@FindBy (xpath="//a[.='FI-FW-02']")
	WebElement button_goldfish;
	
	
}