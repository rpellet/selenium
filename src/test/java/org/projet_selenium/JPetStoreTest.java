package org.projet_selenium;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class JPetStoreTest {
	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/driver/geckodriver.exe");	
	}
	
	@After
	public void tearDown() {
	
	}
	
	@Test
	public void test() {
		WebDriver driver = new FirefoxDriver();	
		
		// ouverture du jpetstore 
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		driver.getTitle().contentEquals("JPetStore Demo");

		// connexion de l'utilisateur
	    driver.findElement(By.xpath("//a[contains(text(), 'Sign In')]")).click();
	    driver.findElement(By.xpath("//input[@name='username']")).sendKeys("j2ee");
	    driver.findElement(By.xpath("//input[@name='password']")).clear();
	    driver.findElement(By.xpath("//input[@name='password']")).sendKeys("j2ee");
	    driver.findElement(By.xpath("//input[@name='signon']")).click();
	    driver.findElement(By.xpath("//div[contains(text(), 'Welcome ABC!')]"));
	    
	    // click sur le lien Fish
	    driver.findElement(By.xpath("//div[@id='SidebarContent']/a[contains(@href,'categoryId=FISH')]")).click();
	    driver.findElement(By.xpath("//h2[.='Fish']"));
	    
	    // sélection d'un produit
	    List<WebElement> listeProduits = driver.findElements(By.xpath("//a[contains(@href,'viewProduct')]"));
	    listeProduits.get(0).click();
	    
	    // sélection d'un item à ajouter au panier
	    List<WebElement> listeItems = driver.findElements(By.xpath("//a[.='Add to Cart']"));
	    listeItems.get(0).click();
	    
	    // modification de la quantité de l'item dans le panier
	    driver.findElement(By.xpath("//input[@name!='keyword' and @type='text']")).clear();
	    driver.findElement(By.xpath("//input[@name!='keyword' and @type='text']")).sendKeys("2");
	    driver.findElement(By.xpath("//input[@name='updateCartQuantities']")).click();
	    
	    // vérification du total du panier
	    List<WebElement> listePrix = driver.findElements(By.xpath("//td[contains(.,'$')]"));
	    String prixItem = listePrix.get(0).getText().substring(1);
	    String totalPanier = listePrix.get(1).getText().substring(1);
	    
	    Float totalPanierAttendu = Float.parseFloat(prixItem)*2;
	    Float totalPanierAffiche = Float.parseFloat(totalPanier);
	    
	    System.out.println("==== prix unitaire : " + prixItem);
	    System.out.println("==== total calculé : " + totalPanierAttendu);
	    System.out.println("==== total affiché : " + totalPanierAffiche);
	    
	    assertEquals(totalPanierAttendu, totalPanierAffiche);
	    
		
		driver.quit();
	}
}