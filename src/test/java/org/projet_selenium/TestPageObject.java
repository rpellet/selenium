package org.projet_selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

public class TestPageObject {
	
	private WebDriver driver;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
	
	@Test
	public void test() {
		
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		
		PageIndex page_index = PageFactory.initElements(driver, PageIndex.class);
		
		PageLogin page_login = page_index.clickButtonSignin(driver);
		
		Homepage homepage = page_login.Login(driver, "j2ee", "j2ee");
		
		PageMyAccount page_myAccount = homepage.clickButtonMyAccount(driver);
		page_myAccount.changeLanguageTo(driver, "japanese");
		page_myAccount.changeFavouriteCategoryTo(driver, "REPTILES");
		
		System.out.println("==== is MyList enabled: " + page_myAccount.box_enableMyList.isSelected());
		System.out.println("==== is MyBanner enabled: " + page_myAccount.box_enableMyBanner.isSelected());
		
		page_myAccount.clickBoxEnableMyList(driver);
		page_myAccount.clickButtonSaveAccountInformation(driver);
		
		System.out.println("==== is MyList enabled: " + page_myAccount.box_enableMyList.isSelected());
		System.out.println("==== is MyBanner enabled: " + page_myAccount.box_enableMyBanner.isSelected());
		System.out.println("==== language preference: " + page_myAccount.getSelectedLanguage(driver));
		System.out.println("==== favourite category: " + page_myAccount.getFavouriteCategory(driver));
	}
}