package org.projet_selenium;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SeleniumBasicsTest 
{
	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/driver/geckodriver.exe");	
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void test() {
		WebDriver driver = new FirefoxDriver();	
		
		driver.get("https://latavernedutesteur.fr/");
		System.out.println("==== Titre de la page : " + driver.getTitle());
		
		driver.findElement(By.linkText("LES PLUS LUS")).click();
		System.out.println("==== Titre de la page : " + driver.getTitle());
		
		driver.quit();
	}
}
