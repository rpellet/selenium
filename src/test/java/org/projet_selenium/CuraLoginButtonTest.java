package org.projet_selenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CuraLoginButtonTest {
	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/driver/geckodriver.exe");	
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void test() {

		WebDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// ouverture de la page d'accueil
		driver.get("https://katalon-demo-cura.herokuapp.com/");
		
		// click sur bouton Make Appointment
		driver.findElement(By.xpath("//a[@id='btn-make-appointment']")).click();
		
		// login
		driver.findElement(By.xpath("//input[@id='txt-username']")).sendKeys("John Doe");
		driver.findElement(By.xpath("//input[@id='txt-password']")).sendKeys("ThisIsNotAPassword");
		driver.findElement(By.xpath("//button[@id='btn-login']")).click();
		
		driver.findElement(By.xpath("//button[@id='btn-book-appointment']"));
		
		driver.quit();
	}
}