package org.projet_selenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CuraLoginMenuTest {
	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/driver/geckodriver.exe");	
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void test() {
		WebDriver driver = new FirefoxDriver();	
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		// ouverture de la page d'accueil
		driver.get("https://katalon-demo-cura.herokuapp.com/");
		
		// click sur le menu
		driver.findElement(By.xpath("//a[@id='menu-toggle']")).click();
		
		// attendre que le bouton soit Login visible
		WebElement loginBtn = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[.='Login']")));
		loginBtn.click();
		
		// login
		driver.findElement(By.xpath("//input[@id='txt-username']")).sendKeys("John Doe");
		driver.findElement(By.xpath("//input[@id='txt-password']")).sendKeys("ThisIsNotAPassword");
		driver.findElement(By.xpath("//button[@id='btn-login']")).click();
		
		driver.quit();
	}
}